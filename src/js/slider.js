export default function slider() {
    let slides = document.querySelectorAll(".slider__item");
    let slideIndex = 1;
    showSlides(slideIndex);

    let plusSlides = (n) => {
        showSlides(slideIndex += n);
    }


    function showSlides(n) {

        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (let i = 0; i < slides.length; i++) {
            slides[i].classList.add('slider__item--hide');
        }

        slides[slideIndex - 1].classList.remove('slider__item--hide')
    }

    let prev = document.getElementById('prev');
    let next = document.getElementById('next');
    prev.addEventListener('click', () => {
        plusSlides(1);
    })

    next.addEventListener('click', () => {
        plusSlides(-1);
    })
}