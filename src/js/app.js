import menu from './menu';
import popupForm from './popup';
import slider from './slider';
import header from './header';

menu();
slider();
popupForm();
header();