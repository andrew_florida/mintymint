export default function popupForm() {
    let openFormButton = document.getElementById('openModal');
    let closeFormButton = document.getElementById('closeModal');
    let modal = document.getElementById('modal');
    
    openFormButton.addEventListener('click', function(){
        modal.classList.add("modal--show");
    })

    closeFormButton.addEventListener('click', function () {
        modal.classList.remove("modal--show");
    })

    document.onkeydown = function (evt) {
        if (modal.classList.contains("modal--show")) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                modal.classList.remove("modal--show");
            }
        }
    };
}