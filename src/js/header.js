export default function header() {
    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
        let currentScrollPos = window.pageYOffset;
        let header = document.querySelector("header");
        if (prevScrollpos > currentScrollPos) {
            header.style.top = "0";
        } else {
            header.style.top = "-100px";
        }
        prevScrollpos = currentScrollPos;
    }
}