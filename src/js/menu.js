export default function menu() {
    let mobileMenuButton = document.getElementById('mobileMenuButton');
    function responsiveMenu() {
        let top = document.getElementById("topnav");
        top.className === "topnav" ? top.className += " responsive" : top.className = "topnav";
        mobileMenuButton.classList.toggle("menu__icon--close");
    }

    mobileMenuButton.addEventListener('click', responsiveMenu);  
}
