const path = require('path');
const webpack = require('webpack');

//plugins
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ExtractTextPlugin = require('extract-text-webpack-plugin');

let isProduction = [process.env.NODE_ENV === 'production'];

module.exports = {
    context: path.resolve(__dirname, 'src'),

    //entry points js
    entry: {
        app: [
            './js/app.js',
            './scss/style.scss'
        ],
    },

    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '../'
    },

    //devServer configuration
    devServer: {
        compress: true,
        port: 9000
    },

    devtool: (isProduction) ? '' : "inline-source-map",
    module: {
        rules: [
            //scss
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: { sourceMap: true }
                        },
                        {
                            loader: 'postcss-loader',
                            options: { sourceMap: true }
                        },
                        {
                            loader: 'sass-loader',
                            options: { sourceMap: true }
                        }
                    ],
                    fallback: 'style-loader'
                })
            },

            //image
            {
                test: /\.(png|gif|jpe?g|svg)$/,
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]'
                        },
                    },
                    'img-loader',
                ]
            },

            //fonts
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                        }
                    }
                ]
            },

            // //svg
            // {
            //     test: /\.svg/,
            //     use: {
            //         loader: 'svg-url-loader',
            //         options: {}
            //     }
            // },
        ],
    },

    plugins: [
        new ExtractTextPlugin(
            './css/[name].css'
        ),

        //new CleanWebpackPlugin(),

        new CopyWebpackPlugin([
            {
                from: './img',
                to: 'img',
            },
            {
                from: './fonts',
                to: 'fonts'
            }
        ]
        )
    ],

    mode: 'production'
}

//PRODUCTION ONLY
if(isProduction){
    module.exports.plugins.push(
        new UglifyJSPlugin({
            sourceMap: true
        }),
    );
    
    module.exports.plugins.push(
        new ImageminPlugin({ test: /\.(jpe?g|png|gif)$/i })
    );

    module.exports.plugins.push(
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
    );
}