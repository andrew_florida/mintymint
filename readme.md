<h2>Folder and file structure</h2>
<pre><code>
├── dist/                      # Build (autogenerate)
│   ├── css/             # styles
│   ├── fonts/           # fonts
│   ├── img/             # images
│   ├── js/              # scripts
├── src/                        # project scripts and files
│   ├── css/             # styles
│   ├── fonts/           # fonts
│   ├── img/             # images
│   ├── js/              # scripts
├── .gitignore           # files that are not added to the remote repository
├── .gitlab-ci.yml       # necessary file to create a remote server on gitlab
├── index.html           # General page
├── package.json         # node modules configurations
├── postcss.config.js    # cross-browser autoprefixer
├── readme.md            # project documentation
└── webpack.conf.js      # webpack.js configuration
</code></pre>

<h3>Install modules</h3>
<pre><code>npm i
</code></pre>

<h3>Run the project - the page will open in the browser at http://localhost:9000/</h3>
<pre><code>npm run start
</code></pre>

<h3>Build to folder <code>dist</code></h3>
<pre><code>npm run build
</code></pre>